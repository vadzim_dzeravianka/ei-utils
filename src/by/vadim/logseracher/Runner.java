package by.vadim.logseracher;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.stream.Stream;

/**
 * 
 * @author Vadzim_Dzeravianko
 *
 */
public class Runner {

  private static final String DEFAULT_PATH = "C:/devei/eap6-conf/log/";
  private static final String DEFAULT_LOG_FILE = "ei-web-booking.log";
  private static final String PATH_TO_GRABED_FILES = "C:\\devei\\TEMP\\SHOPPING\\";

  public static void main(String[] args) {
    System.out.println("START");
    String corralationId = "fbbaab2e-49c9-4fc5-888b-bdca37fde074";
    String ticket = "ASM-1112";
    String path = DEFAULT_PATH;
    String fileName = DEFAULT_LOG_FILE;
    if (args.length > 0) {
      corralationId = args[0];
    }
    if (args.length > 1) {
      fileName = args[1];
    }
    if (args.length > 2) {
      fileName = args[2];
    }
    if (args.length > 3) {
      path = args[3];
    }

    try (Scanner s = new Scanner(new File(path + fileName))) {
      String key1 = ":" + corralationId + ":";
      String key2 = "correlationID[" + corralationId + "]";
      String key3 = " UniqueId=\"" + corralationId + "\"";
      boolean nextLine = false;
      while (s.hasNext()) {
        String line = s.nextLine();
        try {
          if (nextLine && !line.startsWith(" correlationID")) {
            System.out.println(line);
          } else {
            if (line.contains(key3)
                || ((line.contains("ReservationAddSvRQ") || line.contains("ReservationAddSvRS")) && line.contains(key2))) {
              if (!line.contains("AsyncLoggerImpl")) {
                continue;
              }

              String xml = line.substring(line.indexOf("com.aerlingus.asynclogger.AsyncLoggerImpl")
                  + "com.aerlingus.asynclogger.AsyncLoggerImpl".length() + 58);
              saveToFile(xml, PATH_TO_GRABED_FILES + ticket + "\\" + xml.substring(xml.indexOf(":") + 1, xml.indexOf(" ")) + ".xml");
              System.out.println(xml);

              nextLine = true;
            } else {
              nextLine = false;
            }
          }
        } catch (Exception e) {
          e.printStackTrace();
          System.err.println(line);
        }
      }
    } catch (FileNotFoundException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }
    System.out.println("END");
  }

  private static Path saveToFile(String xml, String pathToNewFile) {
    Path p = Paths.get(pathToNewFile);

    try {
      Files.createDirectories(p.getParent());
      Files.deleteIfExists(p);
      Files.createFile(p);
    } catch (Exception e) {
      e.printStackTrace();
    }
    try (BufferedWriter writer = Files.newBufferedWriter(p)) {

      writer.write(xml);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return p;
  }

}
